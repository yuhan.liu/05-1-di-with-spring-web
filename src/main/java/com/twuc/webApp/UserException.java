package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.rmi.NoSuchObjectException;

@ControllerAdvice
public class UserException {

    @ExceptionHandler(NoSuchObjectException.class)
    public ResponseEntity<String> responseNoFoundException(NoSuchObjectException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(exception.getMessage());
    }
}

