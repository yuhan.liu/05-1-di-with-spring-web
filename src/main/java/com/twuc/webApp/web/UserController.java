package com.twuc.webApp.web;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.rmi.NoSuchObjectException;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public GetUserResponse get(@PathVariable Long id) throws NoSuchObjectException {
        GetUserResponse response = service.getUser(id)
                .orElse(null);
        if (null == response) throw new NoSuchObjectException(String.format("No such user: %d", id));
        return response;
    }
}
