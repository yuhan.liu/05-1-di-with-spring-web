package com.twuc.webApp.service;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.domain.UserRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserService {
    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }


    public Optional<GetUserResponse> getUser(Long id) {
        return repository.findById(id).map(GetUserResponse::new);
    }
}
